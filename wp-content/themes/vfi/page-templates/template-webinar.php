<?php
/**

 * Template Name: WebinarPage Template

 */
get_header(); ?>
<ul id="myList">
<?PHP 
$cnt = 0;
$add =0;

 $args = array('post_type' => 'webinar','order'=>'DESC','orderby' => 'title','posts_per_page'=>'-1');
 $loop = new WP_Query( $args );
 while ( $loop->have_posts() ) : $loop->the_post();
  if($cnt%6 == 0)
    {
        $add++;
    }
?>
    <li id="<?php echo $add; ?>">
      <a href="<?php echo get_permalink(get_the_ID());?>"><?php echo the_post_thumbnail('thumbnail'); ?></a><br />
      <?php echo get_the_date( 'm/d/Y' )." at ". get_the_time('G:ia');?><br />
      <?php the_title();?><br />
      <?php the_excerpt(); ?>
      <a href="<?php echo get_permalink(get_the_ID());?>">LEARN MORE</a><br />
    </li>
    <?php $cnt++;  endwhile; ?>  
</ul>
<div id="loadMore">Show More Webinars</div>

<script type="text/javascript">
jQuery(document).ready(function() {
click = 1;
size_li = jQuery("#myList li").size();
total = Math.ceil(size_li/6);

    jQuery("#loadMore").click(function(){
        click = click +1;
        if(click <= total){
        jQuery('#myList li').hide();
        jQuery('#myList #'+click).show();
        }
        else{
        jQuery('#myList li').hide();
        jQuery('#myList #1').show();
        click =1 ;
            
        }
    });
    jQuery('#myList li').hide();
    jQuery('#myList #1').show();
 
 });  
</script>
<?php get_footer(); ?>






