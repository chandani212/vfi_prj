<?php
/**

 * Template Name: HomePage Template

 */
get_header(); ?>
<?php
	if( have_rows('home_page_all_section') ){
        while ( have_rows('home_page_all_section') ) { the_row();
                		
			$scroller_layout = get_row_layout();
   
			if( $scroller_layout ){
			  	get_template_part( 'content/home_page_section/section', $scroller_layout ); 
			}
        }
 } 
?>
<?php get_footer(); ?>

