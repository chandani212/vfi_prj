<?php
/**

 * Template Name: BlogPage Template

 */
get_header(); ?>

<h1><?php echo  the_title(); ?></h1>

    <select name="archive-dropdown" onChange='document.location.href=this.options[this.selectedIndex].value;'>
    <option value=""><?php echo attribute_escape(__('Archive')); ?></option>
    <?php wp_get_archives('type=monthly&format=option&show_post_count=0'); ?>
    </select> 

    <?php 
    global $wpdb;
    $authors = $wpdb->get_results("SELECT ID,user_login, user_nicename,display_name from $wpdb->users ORDER BY display_name");
    ?>

    <form method="post">
          <select id="regions" style="color:red;background-color: white;" >
                   <option value="">Select Author</option>
                   <?php foreach($authors as $author) :?>
                   <option value="<?php echo site_url().'/author/'. $author->user_login;?>" ><?php echo $author->display_name;?></option> 
                    <?php  
                    endforeach; ?>
          </select>
             
          <div class="row retailers" id="retailers"></div>
    </form>
    <?php  $args = array('post_type' => '','posts_per_page'=>'5','order'=>'DESC','orderby' => 'title','paged' => $paged);
           $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();?>
                    <?php
                          
                          $number=(int) get_comments_number( get_the_ID() );
                            if ( 0 === $number )
                                $css_class = 'zero-comments';
                            elseif ( 1 === $number )
                                $css_class ='one-comments';    
                            elseif ( 1 < $number )
                                $css_class = 'multiple-comments';
                    ?>
                      <a href="<?php echo get_permalink(get_the_ID());?>"><?php echo the_post_thumbnail('thumbnail'); ?></a><br />
                      <a href="<?php echo get_permalink(get_the_ID());?>" target="_blank">READ MORE</a>
                      <a href="<?php echo get_permalink(get_the_ID());?>" target="_blank" class="<?php echo $css_class;?>" id="comment" >
                 	  <?php	comments_popup_link( 'Add Your Reaction', '1 Reactions', '% Reactions', $css_class, 'Comments are off for this post');	?>			
        	         </a>
                      <a href="<?php echo get_permalink(get_the_ID()); ?>"><h1 class="entry-title"><?php the_title(); ?></h1></a>
                      <?php twentythirteen_entry_meta(); ?>
                      <?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
                      <?php //the_content(); 
                      the_excerpt();  ?>
                      <hr />
                      
             <?php  endwhile;
	      wp_reset_query();
           
             $total_pages = $loop->max_num_pages;
          
        if ($loop->max_num_pages > 1) : // check if the max number of pages is greater than 1  ?>
          <nav class="pagination">
                  <?php 
                   $total_pages = $loop->max_num_pages;
                   if ($total_pages > 1){
                        $current_page = max(1, get_query_var('paged'));
                        echo paginate_links(array(
                            'base' =>  get_pagenum_link(1) . '%_%',
                            'format' => '/page/%#%',
                            'current' => $current_page,
                            'total' => $total_pages,
                        ));
                    }
                  ?>
          </nav>
        <?php else: ?>
                  <article>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                  </article>
        <?php endif; ?>
<hr />
<?php get_footer(); ?>

<script>
    jQuery(document).ready(function() {
      jQuery('#regions').on('change', function () {
          var url = jQuery(this).val(); 
          if (url) { 
                window.open (url ,'_self');
          }
          return false;
      });
    });
</script>






