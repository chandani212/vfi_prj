<?php
/**
 * Template Name: Issues Template
*/
get_header(); ?>
 <?php   $args = array('post_type' => 'issues','posts_per_page'=>'4','order'=>'DESC','orderby' => 'title');
                  $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();?>
                      <a href="<?php echo get_permalink(get_the_ID()); ?>"><h1 class="entry-title"><?php the_title(); ?></h1></a>
                      <a href="<?php echo get_field('detail_page_link',$post->ID);?>"><?php echo the_post_thumbnail('thumbnail'); ?></a>
                      <?php the_content(); ?>
      <?php  endwhile;
      wp_reset_query();
?>  
<?php get_footer();?>

<?php

// pagination code 
   /*     if( get_query_var('page') ) 
        { $page = get_query_var( 'page' );} 
        else 
        {$page = 1;}   
             
        $row              = 0;
        $images_per_page  = 3; // How many images to display on each page
        $images           = get_field( 'issue_post' );
        $total            = count( $images );
        $pages            = ceil( $total / $images_per_page );
        $min              = ( ( $page * $images_per_page ) - $images_per_page ) + 1;
        $max              = ( $min + $images_per_page ) - 1;
        

        $i= get_field('issue_post');
            if( $i ): ?>
                    <ul>
                    <?php foreach( $i as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php 
                           $row++;
                          if($row < $min) { continue; } 
                           if($row > $max) { break; } 
                     ?>
                        <?php setup_postdata($post); ?>
                        <li>
                           <a href="<?php echo get_permalink(get_the_ID()); ?>"><h1 class="entry-title"><?php echo $post->post_title; ?></h1></a>
                           <a href="<?php echo get_field('detail_page_link',$post->ID);?>"><?php echo the_post_thumbnail('thumbnail',$post->ID); ?></a>
                           <?php  echo $post->post_content; ?>
                        </li>
                    <?php endforeach; 
            endif;
    echo paginate_links(array('base' => get_permalink() . '%#%' . '/','format' => '?page=%#%','current' => $page,'total' => $pages));
 */
 ?>