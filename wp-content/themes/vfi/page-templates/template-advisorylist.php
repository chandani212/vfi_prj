<?php
/**
* Template Name: Advisorylist Page Template
*/
get_header(); ?>
<?php
global $post;
 $args = array('post_type' => 'advisory','order'=>'ASC','orderby' => 'meta_value','posts_per_page'=>'-1', 'meta_key' => 'last_name');//DESC
 $loop = new WP_Query( $args );
 while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <a href="<?php echo get_permalink(get_the_ID());?>"><?php echo the_post_thumbnail('thumbnail'); ?></a><br />
      <?php echo  get_field('first_name');?>
      <?php echo get_field('last_name');?><br />
      <?php echo get_field('job_title');?><br />
      <?php echo get_field('business_name');?><br />
      
<?php endwhile;?>
<?php get_footer(); ?>
