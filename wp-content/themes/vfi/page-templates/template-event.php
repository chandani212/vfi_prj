<?php
/**

 * Template Name: Event Page Template

 */
get_header(); ?>
<?php
global $post;
 $args = array('post_type' => 'event','order'=>'DESC','orderby' => 'title','posts_per_page'=>'-1');
 $loop = new WP_Query( $args );

 
 while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <h3><?php the_title();?></h3>
      <h4><?php echo get_the_date( 'm/d/Y' )." at ". get_the_time('G:ia');?>
      <?php echo get_field('location');?></h4><br/>
      <?php echo the_content();?>
      <a href="<?php echo get_permalink(get_the_ID());?>"><?php echo the_post_thumbnail('thumbnail'); ?></a><br />
      <button name="rsvp" value="RSVP">RSVP</button> 
<?php endwhile;?>
<?php get_footer(); ?>

