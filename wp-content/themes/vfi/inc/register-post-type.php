<?php
/*
 * Register Programs Post Type
 */
add_action('init','vfi_create_post_type');

function vfi_create_post_type(){
    
	// member Post type register 
	$member_args_array = array(
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'),
		'menu_position' =>8,
		'exclude_from_search' => false,
		//'menu_icon' => 'dashicons-format-Member',
		'label'              => 'Webinar',	
		'public' => true,
		'show_ui' => true,	
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'webinar'),
		'query_var' => true,
		'has_archive' => true,
		'show_in_menu'	=> true,
		'labels'              => array(
			'name'              => __( 'Webinar', 'vfi' ),
			'singular_name'     => __( 'Webinar', 'vfi' ),
			'add_new'           => __( 'Add New', 'vfi' ),
			'add_new_item'      => __( 'Add New Webinar', 'vfi' ),
			'edit_item'         => __( 'Edit Webinar', 'vfi' ),
			'new_item'          => __( 'New Webinar', 'vfi' ),
			'view_item'         => __( 'View Webinar', 'vfi' ),
			'all_items'          => __( 'All Webinar', 'vfi' ),
			'search_items'      => __( 'Search Webinar', 'vfi' ),
			'not_found'         => __( 'No Webinar found', 'vfi' ),
			'not_found_in_trash'=> __( 'No Webinar in Trash', 'vfi' ),
			'parent_item_colon' => __( 'Parent Webinar:', 'vfi' ),
			'menu_name'         => __( 'Webinar', 'vfi' ),
		),
		
	);
	register_post_type( 'webinar', $member_args_array );	
	/* END member Post type register */

	/* Our issue Post type register */
	$ourissue_args_array = array(
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
		'menu_position' =>9,
		'exclude_from_search' => false,
	
		'label'   => 'Our Issues',	
		'public' => true,
		'show_ui' => true,	
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'issues'),
		'query_var' => true,
		'has_archive' => true,
		'show_in_menu'	=> true,
		'labels'              => array(
			'name'              => __( 'Issues', 'vfi' ),
			'singular_name'     => __( 'Issues','vfi' ),
			'add_new'           => __( 'Add New', 'vfi' ),
			'add_new_item'      => __( 'Add New Issues', 'vfi' ),
			'edit_item'         => __( 'Edit Issues', 'vfi' ),
			'new_item'          => __( 'New Issues', 'vfi' ),
			'view_item'         => __( 'View Issues', 'vfi' ),
			'all_items'          => __( 'All Issues', 'vfi' ),
			'search_items'      => __( 'Search Issues', 'vfi' ),
			'not_found'         => __( 'No Issues found', 'vfi' ),
			'not_found_in_trash'=> __( 'No Issues in Trash', 'vfi' ),
			'parent_item_colon' => __( 'Parent Issues:', 'vfi' ),
			'menu_name'         => __( 'Issues', 'vfi' ),
		),
		
	);
	register_post_type( 'issues', $ourissue_args_array );
	/* END Our issue Post type register */
    
	/* Featured Content Post type register */
	$ourissue_args_array = array(
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
		'menu_position' =>9,
		'exclude_from_search' => false,
	
		'label'   => 'Featured Content',	
		'public' => true,
		'show_ui' => true,	
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'featuredcontent'),
		'query_var' => true,
		'has_archive' => true,
		'show_in_menu'	=> true,
		'labels'              => array(
			'name'              => __( 'Featured Content', 'vfi' ),
			'singular_name'     => __( 'Featured Content','vfi' ),
			'add_new'           => __( 'Add New', 'vfi' ),
			'add_new_item'      => __( 'Add New Featured Content', 'vfi' ),
			'edit_item'         => __( 'Edit Featured Content', 'vfi' ),
			'new_item'          => __( 'New Featured Content', 'vfi' ),
			'view_item'         => __( 'View Featured Content', 'vfi' ),
			'all_items'          => __( 'All Featured Content', 'vfi' ),
			'search_items'      => __( 'Search Featured Content', 'vfi' ),
			'not_found'         => __( 'No Featured Content found', 'vfi' ),
			'not_found_in_trash'=> __( 'No Featured Content in Trash', 'vfi' ),
			'parent_item_colon' => __( 'Parent Featured Content:', 'vfi' ),
			'menu_name'         => __( 'Featured Content', 'vfi' ),
		),
		
	);
	register_post_type( 'featured_content', $ourissue_args_array );
    
    // Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => __( 'Featured Content Category', 'tribefm' ),
		'singular_name'     => __( 'Featured Content Category', 'tribefm' ),
		'search_items'      => __( 'Search Featured Content Category','tribefm' ),
		'all_items'         => __( 'All Featured Content Category' ,'tribefm'),
		'parent_item'       => __( 'Parent Featured Content Category' ,'tribefm'),
		'parent_item_colon' => __( 'Parent Featured Content Category:','tribefm' ),
		'edit_item'         => __( 'Edit Featured Content Category', 'tribefm' ),
		'update_item'       => __( 'Update Featured Content Category', 'tribefm' ),
		'add_new_item'      => __( 'Add New Featured Content Category', 'tribefm' ),
		'new_item_name'     => __( 'New Featured Content Category', 'tribefm' ),
		'menu_name'         => __( 'Featured Content Category', 'tribefm' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'featuredcontentcategory' ),
	);

	register_taxonomy( 'featuredcontentcategory', 'featured_content', $args );
	/* END  Featured Content Post type register */
	
	/* Story Post type register */
	$story_args_array = array(
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
		'menu_position' =>6,
		'exclude_from_search' => false,
		'label'              => 'Stories',	
		'public' => true,
		'show_ui' => true,	
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'stories'),
		'query_var' => true,
		'has_archive' => false,
		'show_in_menu'	=> true,
		'labels'              => array(
			'name'              => __( 'Stories', 'vfi' ),
			'singular_name'     => __( 'Stories','vfi' ),
			'add_new'           => __( 'Add New', 'vfi' ),
			'add_new_item'      => __( 'Add New Stories', 'vfi' ),
			'edit_item'         => __( 'Edit Stories', 'vfi' ),
			'new_item'          => __( 'New Stories', 'vfi' ),
			'view_item'         => __( 'View Stories', 'vfi' ),
			'all_items'          => __( 'All Stories', 'vfi' ),
			'search_items'      => __( 'Search Stories', 'vfi' ),
			'not_found'         => __( 'No Stories found', 'vfi' ),
			'not_found_in_trash'=> __( 'No Stories in Trash', 'vfi' ),
			'parent_item_colon' => __( 'Parent Stories:', 'vfi' ),
			'menu_name'         => __( 'Stories', 'vfi' ),
		),
		
	);
	register_post_type( 'stories', $story_args_array );
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => __( 'Stories Category', 'tribefm' ),
		'singular_name'     => __( 'Stories Category', 'tribefm' ),
		'search_items'      => __( 'Search Stories Category','tribefm' ),
		'all_items'         => __( 'All Stories Category' ,'tribefm'),
		'parent_item'       => __( 'Parent Stories Category' ,'tribefm'),
		'parent_item_colon' => __( 'Parent Stories Category:','tribefm' ),
		'edit_item'         => __( 'Edit Stories Category', 'tribefm' ),
		'update_item'       => __( 'Update Stories Category', 'tribefm' ),
		'add_new_item'      => __( 'Add New Stories Category', 'tribefm' ),
		'new_item_name'     => __( 'New Stories Category', 'tribefm' ),
		'menu_name'         => __( 'Stories Category', 'tribefm' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'storiescategory' ),
	);

	register_taxonomy( 'storiescategory', 'stories', $args );
	/* END Story Post type register */
	
    
    /* Advisory Task Force Post type register */
	$advisory_args_array = array(
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
		'menu_position' =>6,
		'exclude_from_search' => false,
		'label'              => 'Advisory Task Force',	
		'public' => true,
		'show_ui' => true,	
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'advisory'),
		'query_var' => true,
		'has_archive' => false,
		'show_in_menu'	=> true,
		'labels'              => array(
			'name'              => __( 'Advisory Task Force', 'vfi' ),
			'singular_name'     => __( 'Advisory Task Force','vfi' ),
			'add_new'           => __( 'Add New', 'vfi' ),
			'add_new_item'      => __( 'Add New Advisory Task Force', 'vfi' ),
			'edit_item'         => __( 'Edit Advisory Task Force', 'vfi' ),
			'new_item'          => __( 'New Advisory Task Force', 'vfi' ),
			'view_item'         => __( 'View Advisory Task Force', 'vfi' ),
			'all_items'          => __( 'All Advisory Task Force', 'vfi' ),
			'search_items'      => __( 'Search Advisory Task Force', 'vfi' ),
			'not_found'         => __( 'No Advisory Task Force found', 'vfi' ),
			'not_found_in_trash'=> __( 'No Advisory Task Force in Trash', 'vfi' ),
			'parent_item_colon' => __( 'Parent Advisory Task Force:', 'vfi' ),
			'menu_name'         => __( 'Advisory Task Force', 'vfi' ),
		),
		
	);
	register_post_type( 'advisory', $advisory_args_array );
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => __( 'Advisory Task Force Category', 'tribefm' ),
		'singular_name'     => __( 'Advisory Task Force Category', 'tribefm' ),
		'search_items'      => __( 'Search Advisory Task Force Category','tribefm' ),
		'all_items'         => __( 'All Advisory Task Force Category' ,'tribefm'),
		'parent_item'       => __( 'Parent Advisory Task Force Category' ,'tribefm'),
		'parent_item_colon' => __( 'Parent Advisory Task Force Category:','tribefm' ),
		'edit_item'         => __( 'Edit Advisory Task Force Category', 'tribefm' ),
		'update_item'       => __( 'Update Advisory Task Force Category', 'tribefm' ),
		'add_new_item'      => __( 'Add New Advisory Task Force Category', 'tribefm' ),
		'new_item_name'     => __( 'New Advisory Task Force Category', 'tribefm' ),
		'menu_name'         => __( 'Advisory Task Force Category', 'tribefm' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'advisorycategory' ),
	);

	register_taxonomy( 'advisorycategory', 'advisory', $args );
	/* END Story Post type register */
    
      /* Event Post type register */
	$event_args_array = array(
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
		'menu_position' =>6,
		'exclude_from_search' => false,
		'label'              => 'Event',	
		'public' => true,
		'show_ui' => true,	
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'event'),
		'query_var' => true,
		'has_archive' => false,
		'show_in_menu'	=> true,
		'labels'              => array(
			'name'              => __( 'Event', 'vfi' ),
			'singular_name'     => __( 'Event','vfi' ),
			'add_new'           => __( 'Add New', 'vfi' ),
			'add_new_item'      => __( 'Add New Event', 'vfi' ),
			'edit_item'         => __( 'Edit Event', 'vfi' ),
			'new_item'          => __( 'New Event', 'vfi' ),
			'view_item'         => __( 'View Event', 'vfi' ),
			'all_items'          => __( 'All Event', 'vfi' ),
			'search_items'      => __( 'Search Event', 'vfi' ),
			'not_found'         => __( 'No Event found', 'vfi' ),
			'not_found_in_trash'=> __( 'No Event in Trash', 'vfi' ),
			'parent_item_colon' => __( 'Parent Event:', 'vfi' ),
			'menu_name'         => __( 'Event', 'vfi' ),
		),
		
	);
	register_post_type( 'event', $event_args_array );
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => __( 'Event Category', 'tribefm' ),
		'singular_name'     => __( 'Event Category', 'tribefm' ),
		'search_items'      => __( 'Search Event Category','tribefm' ),
		'all_items'         => __( 'All Event Category' ,'tribefm'),
		'parent_item'       => __( 'Parent Event Category' ,'tribefm'),
		'parent_item_colon' => __( 'Parent Event Category:','tribefm' ),
		'edit_item'         => __( 'Edit Event Category', 'tribefm' ),
		'update_item'       => __( 'Update Event Category', 'tribefm' ),
		'add_new_item'      => __( 'Add New Event Category', 'tribefm' ),
		'new_item_name'     => __( 'New Event Category', 'tribefm' ),
		'menu_name'         => __( 'Event Category', 'tribefm' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'eventcategory' ),
	);

	register_taxonomy( 'eventcategory', 'event', $args );
	/* END Story Post type register */
}
?>