<hr />
<?php if (get_field('display_section') == 'Yes') { 
    ////Featureed content title  Section start   ?>
    <div class="get_involved" style="background-image: url('<?php  $image = get_field('background_image'); echo $image['url']; ?>') no-repeat 0 0;">
        <?php $args = array('post_type' => 'featured_content','posts_per_page'=>'1','order'=>'DESC','orderby' => 'title');
                  $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();?>
                      <h1 class="entry-title"><?php the_title(); ?></h1>
                      <?php echo the_post_thumbnail('medium'); ?>
                      <?php the_content(); ?>
             <?php  endwhile;
               wp_reset_query();?>
               <a href="<?php echo get_field('detail_page_link'); //echo post_permalink( get_the_ID() );  ?>">Read more</a>
                <?php if( have_rows('featured_content_button') ):
                        while ( have_rows('featured_content_button') ) : the_row(); ?><br />
                     <!--<input   type="button" value="<?php echo get_sub_field('button_title');?>" onclick="<?php echo get_sub_field('button_url');?>"/> -->
                        <a href="<?php echo get_sub_field('button_url');?>"  target="_blank"><?php echo get_sub_field('button_title');?> </a>
                <?php    endwhile;
                     endif;?>
   </div>
  <?php } //Featureed content title Section end  ?>
