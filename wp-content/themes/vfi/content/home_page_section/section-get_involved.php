<hr />
<?php //Get Involved Section Start ?>
<div class="get_involved" style="background-image: url('<?php  $image = get_field('background_image'); echo $image['url']; ?>') no-repeat 0 0;">
         <h2><?php echo get_field('get_involved_title'); ?></h2>
         <p><?php echo get_field('description');?></p>
           <a href="<?php echo get_field('sign_the_petition');?>"  target="_blank"><?php //echo get_field('sign_the_petition');?> sign the petition </a> &nbsp;&nbsp;
           <a href="<?php echo get_field('send_a_letter');?>"  target="_blank"><?php //echo get_field('send_a_letter');?> send a letter</a>&nbsp;&nbsp;
           <a href="<?php echo get_field('tweet_to_congress');?>"  target="_blank"><?php //echo get_field('tweet_to_congress');?>tweet to congress </a>&nbsp;&nbsp;
           <a href="<?php echo get_field('share');?>"  target="_blank"><?php //echo get_field('share');?> share</a>&nbsp;&nbsp;
</div>           
<?php //Get Involved Section end ?>
