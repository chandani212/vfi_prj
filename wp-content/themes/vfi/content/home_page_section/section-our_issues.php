<hr />
<?php
     //Our issues Section Start 
      $args = array('post_type' => 'issues','posts_per_page'=>'2','order'=>'DESC','orderby' => 'title');
                  $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();?>
                      <a href="<?php echo get_permalink(get_the_ID()); ?>"><h1 class="entry-title"><?php the_title(); ?></h1></a>
                      <a href="<?php echo get_field('detail_page_link',$post->ID);?>"><?php echo the_post_thumbnail('thumbnail'); ?></a>
                      <?php the_content(); ?>
      <?php  endwhile;
      wp_reset_query();

      $count_pages = wp_count_posts('issues')->publish; 
        if($count_pages >= 2):?>
            <a href="<?php echo site_url().'/more-issues/'?>">MORE ISSUES</a>
  <?php endif;
   ////Our issues Section  end  ?>    