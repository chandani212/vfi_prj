<hr />
<?php //Exclusive Access Section Start ?>
        <h1><?php echo get_field('exclusive_access_title'); ?></h1>
        <?php 
          if( have_rows('exclusive_access') ):
            while ( have_rows('exclusive_access') ) : the_row();
            ?>
            <h2><?php echo get_sub_field('title'); ?></h2>
            <?php $image= get_sub_field('image');?>
            <img src="<?php echo $image['url'];?>" height="100" width="100" />
            <p><?php echo get_sub_field('short_description');?></p>
            <a href="<?php  echo get_sub_field('learn_more_link');?>">read more</a>
            <?php    
            endwhile;
          endif;
//Exclusive Access Section end
?>