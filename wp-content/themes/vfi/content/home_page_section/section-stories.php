<hr />
<ul class="bxslider">
   <?php $args = array('post_type' => 'stories','posts_per_page'=>'-1','order'=>'DESC','orderby' => 'title');
                  $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();?>
                      <li><?php echo the_post_thumbnail('medium'); ?></li>
                      <li><?php  $video_url= get_field('video_url'); echo $video_url;?></li>			
                      <li><?php the_content(); ?></li>
             <?php  endwhile;
         wp_reset_query();?>
</ul>
<script type="text/javascript">
jQuery('.bxslider').bxSlider({
 // auto: true,
 // autoControls: true,
  minSlides: 3,
  maxSlides: 3,
  slideWidth: 170,
  slideMargin: 10
});
</script>
