<hr />
<?php //Keep Me Updated Section Start ?>
<div class="keep_me_updated" style="background-image: url('<?php  $bkimg = get_field('keep_me_updated_background__image'); echo $bkimg['url']; ?>') no-repeat 0 0;">
         <h2><?php echo get_field('keep_me_updated_title'); ?></h2>
         <img src="<?php $image1 = get_field('keep_me_updated_icon');  echo $image1['url'];?>" height="50" width="50" />
         <?php $code=get_field('contact_form_shortcode'); echo do_shortcode($code);?>
</div>           
<?php //Keep Me Updated Section end ?>