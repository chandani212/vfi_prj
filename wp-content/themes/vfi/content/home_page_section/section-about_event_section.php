<hr />
<div class="about-us" id="about-us">

<?php
//About us section start
$the_query = new WP_Query( 'page_id=10' );
while ( $the_query->have_posts() ) :
	$the_query->the_post();
        the_title();
        the_content();
endwhile;
wp_reset_postdata();
// end About us section start
?>

</div>

<hr />
<div class="event" id="event">

<?php
//Event section start
$args = array('post_type'=> 'event','post_status' => 'publish',
                'meta_query'=> array('relation'=> 'AND', 
                array('key'	=> 'event_start_date','compare'=> '>=','value'=> date('Y-m-d'),'type'=> 'date',),));           
                
$loop = new WP_Query( $args );                
 
 while ( $loop->have_posts() ) : $loop->the_post();?>
     <br/><button> <?php echo get_field('event_start_date');?></button>
	 <?php the_title();
    // echo get_field('location');
 endwhile;
  $count_pages = wp_count_posts('issues')->publish; 
        if($count_pages >= 4):?>
      <a href="<?php echo site_url().'/more-issues/'?>">More Events</a>
<?php 
       endif;
wp_reset_postdata();
// end Event section start
?>

</div>