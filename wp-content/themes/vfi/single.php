<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
            <?php $post_type=$post->post_type;?>
            <header class="entry-header">
		      <?php if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ) : ?>
        		<div class="entry-thumbnail">
        			<?php the_post_thumbnail(); ?>
        		</div>
		      <?php endif; ?>

		      <?php if ( is_single() ) : ?>
		      <h1 class="entry-title"><?php the_title(); ?></h1>
                <?php if($post_type == 'advisory'):?>
                <?php echo  get_field('first_name');?>
                <?php echo get_field('last_name');?><br />
                <?php echo get_field('job_title');?><br />
                <?php echo get_field('business_name');?><br />
                <a href="<?php echo get_field('company_url');?>" target="_blank"><?php echo get_field('company_name');?></a><br />
                <?php endif; ?>
		      <?php else : ?>
      		   <h1 class="entry-title">
        			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
               </h1>
		      <?php endif; // is_single() ?>
                <?php echo  get_the_date( 'm/d/Y' )." at ". get_the_time('G:ia');?>
        		<div class="entry-meta">
        			<?php twentythirteen_entry_meta(); ?>
        			<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
        		</div><!-- .entry-meta -->
            </header><!-- .entry-header -->

        	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
        	<div class="entry-summary">
        		<?php the_excerpt(); ?>
        	</div><!-- .entry-summary -->
        	<?php else : ?>
        	<div class="entry-content">
        		<?php
        			/* translators: %s: Name of current post */
        			the_content( sprintf(
        				__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'twentythirteen' ),
        				the_title( '<span class="screen-reader-text">', '</span>', false )
        			) ); 
                     $support_vfi=get_field('why_i_support_vfi');
                      if($post_type == 'advisory' && !empty($support_vfi)):
                        echo "<h3>Why I Support VFI</h3>";
                        echo  $support_vfi;
                      endif;
        
        			wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
        		?>
        	</div><!-- .entry-content -->
        	<?php endif; ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
  <hr />
 <h3> Related Content</h3>
 <?php $args = array('post_type' => $post_type,'order'=>'DESC','orderby' => 'title','posts_per_page'=>'3');
 $loop = new WP_Query( $args );
 while ( $loop->have_posts() ) : $loop->the_post(); ?>
    <a href="<?php echo get_permalink(get_the_ID());?>"><?php echo the_post_thumbnail('thumbnail'); ?></a><br />
      <?php the_title();?><br />
      <?php the_excerpt(); ?>
      <a href="<?php echo get_permalink(get_the_ID());?>">LEARN MORE</a><br />
<?php endwhile; ?>
<?php get_footer(); ?>