<?php
/**

 * VFI functions and definitions

 *

 * Sets up the theme and provides some helper functions, which are used

 * in the theme as custom template tags. Others are attached to action and

 * filter hooks in WordPress to change core functionality.

 * 

 * @package WordPress

 * @subpackage VFI

 */

add_action( 'after_setup_theme', 'scaffold_setup');
function scaffold_setup()
{
	include_once( 'inc/register-post-type.php' ); 
	include_once( 'inc/twitter-oath-widget.php' );
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}
wp_enqueue_script( 'bxslider-min-js', get_stylesheet_directory_uri() . '/js/jquery.bxslider.min.js', array( 'jquery' ));

?>





