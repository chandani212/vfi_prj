<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vfi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I:5|`qdl[9%5f<8e!eRq$$:`r~!CbY6+5l.n;q1e;hJIY!)Lk*DJWX(j/M*?9E-w');
define('SECURE_AUTH_KEY',  'NxK61iHqwaR@:h}b[=UK1Qif+1a,-2l:+;*e:|2 =;g]j$qpM#QHZ(.s|+jrBX7V');
define('LOGGED_IN_KEY',    '@~W&C{%M>_[&*zPfPp#x|l@n|4^J,W?U0G+(Zqm6C&-2F+T&gDSDM|l?!7p;*?j{');
define('NONCE_KEY',        'z|L$^?6}%q%fj=NJ)u.^OdFc3 #d}um q*RF[h@(]U1[|Qqep%9Fo+$+t$5C@gk@');
define('AUTH_SALT',        '7oTC88I,~QY).7<+J]Cw-g?$>s+D_A$xc[u%^~*tyF+e9gS9L*A%fD$J&HB /.B{');
define('SECURE_AUTH_SALT', '/,J,J*y)#N-+YpGBpGsVuv|zRf&o--GXnh7y(<@+_`1ro};PfuzzZqJKxp;3vR!P');
define('LOGGED_IN_SALT',   '>}sl=cT+s,$guB0-:oDO/$_bSMW,ofz.NX6peNMeiro.&B}VnnO-e-*{9coTD++C');
define('NONCE_SALT',       'ChHJH@o(s(sq|qAj=3Ol5KZC<kbJSW2{g?%dy]|X*MF#I%vH)brv$Mu-TI|>yvMO');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
